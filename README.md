<img src="https://s2.qwant.com/thumbr/0x0/4/d/b25765fdd9b4d665c4dca5fed41492bd83e1e1dc37f49b1d4235496a44f343/attention-1.gif?u=http%3A%2F%2Fhamsterdomestique.e-monsite.com%2Fmedias%2Fimages%2Fattention-1.gif&q=0&b=1&p=0&a=1" style="float: left;"/> 
              ATTENTION : NE TOUCHEZ PAS AU DOSSIER PUBLIC

****************************************************************************************************************************
# AVANT TOUTE CHOSE :


1. Vérifier la version de hugo avec la commande `hugo version`


2. Si votre version est la 0.57 passez votre chemin ;)


3. Désistaller l'anciène version : `sudo apt-get remove hugo` & `sudo snap remove hugo`


4. Installer la dernière version : `sudo snap install hugo`


5. Enfin redémarer votre terminal et faite `hugo version` si 0.57 vous pouvez continuer.


# COMMENT CREE UN POST

**Pré-requis : cloner le dépot sur votre ordinateur.**


## Nous utilisons la technologie hugo serveur : [Hugo Documentation](https://gohugo.io/documentation/)


1. Pour créer un post utilisez la commande `hugo new posts/nom_du_post.md` dans le terminal à l'emplacement du dossier lesimposteurs/.


2. Votre post à été créé dans le dossier /content/posts n'oubliez pas de compléter la description et l'auteur dans le frontmatter.


3. Vous pouvez l'éditer en utilisant le language [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


4. Pour que votre post soit visible sur note site vérifiez que "Draft = false" dans votre /nom_du_post.md


5. Ensuite exécutez la commande `hugo` dans votre console dans lesimposteurs/ (Cela va permettre à hugo de générer votre ajout dans le dossier public).


6. Enfin sauvegardez et pushez votre modification (`git add .` ; `git commit -m "nom_du_post"` ; `git push`)

****************************************************************************************************************************

# COMMENT CREE UNE VEILLE

**Pré-requis : cloner le dépot sur votre ordinateur.**


## Nous utilisons la technologie hugo serveur : [Hugo Documentation](https://gohugo.io/documentation/)


1. Pour créer une veille utilisez la commande `hugo new veilles/nom_de_la_veille.md` dans le terminal à l'emplacement du dossier lesimposteurs/.


2. Votre veille à été créé dans le dossier /content/veilles n'oubliez pas de compléter la description et l'auteur dans le frontmatter.


3. Vous pouvez l'éditer en utilisant le language [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


4. Pour que votre post soit visible sur note site en ligne vérifiez que "Draft = true" dans votre /nom_de_le_veille.md
 

5. Ensuite exécutez la commande `hugo` dans votre console dans lesimposteurs/ (Cela va permettre à hugo de générer votre ajout dans le dossier public).


6. Enfin sauvegardez et pushez votre modification (`git add .` ; `git commit -m "nom_de_la_veille"` ; `git push`)

****************************************************************************************************************************

# UTILISEZ DES IMAGES

1. Placez votre image dans le dossier lesimposteurs/static/img/


2. Dans le contenue de votre fichier.md il vous suffit d'insérer le tag `<img src="/img/votre_image.jpg />"` (Vous pouvez créer un dossier intermédiaire).


3. Vous pouvez modifier la forme en ajoutant une propritété `style = ""`

****************************************************************************************************************************

# "CHARTE" POUR LES VEILLES

1. **Dans le Frontmatter** : 
    - Compétez : 


---
` title: "Nom_de_veille" `

` date: 2019-08-13T14:58:06+02:00 (date de votre veille) `

` draft: false --> toujours marquer false !!! `

` description: "Your description" `

` img: "" --> inclure image sur votre en-tête `

` author: "Votre nom " `
---


2. **Les choses à résumer dans votre rédaction**: 
    - Lien du slide (peut importe ou c'est dans votre post)

    - Avantages / Inconvénients (facultatif selon les veilles)

    - Conclusion - idée générale

    - Liens utiles en utilisant les links Markdown :
   
exemple avec Thingz : ` [Ce que vous voulez marquer dans votre affichage]("https://thingz.co/" - votre link juste après ) `

### Bonne rédaction !