---
title: "Media Queries"
date: 2019-08-09T16:07:48+02:00
draft: false
description: "Rendez votre site reponsive !"
img: ""
author: "Lucas"
---

## C'est quoi ?



Les requêtes média modifient l'apparence d’un site par rapport aux caractéristiques de l’appareil utilisé pour le visionner.


L’affichage sera différent si l’on utilise un écran large, un smartphone, un téléviseur, un aperçu d’impression, etc...


## Comment ça marche ?


On applique certain style de façon conditionnelle grâce aux règles @medias et @import.


Pour cela on leur donne les caractéristiques de l'appareil (hauteur, largeur, affichage des couleurs, présence du curseur, etc…) puis on écrit le style css à appliquer.


## Syntaxe :



#### Types de médias

- all
- print
- screen
- speech
- tv
- handheld
- ...

#### Caractéristiques média

- height
- width
- hover
- color
- pointer
- ...

#### Opérateurs logiques

- And
- Not
- Only
- , (Virgule)



## Quelques liens :


[La Bible](https://developer.mozilla.org/fr/docs/Web/CSS/Requ%C3%AAtes_m%C3%A9dia/Utiliser_les_Media_queries)

[Exemple en vidéo](https://www.youtube.com/watch?v=CFmse9aEUIA)
