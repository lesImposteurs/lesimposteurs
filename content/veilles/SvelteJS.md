---
title: "SvelteJS"
date: 2019-07-17T14:58:06+02:00
draft: false
description: "Veille sur SvelteJS, framework JS orienté composants graphiques + compilateur."
img: ""
author: "Thomas et Marie"
---
*************************************************
## Programme :

- **Présentation** 

- **Objectifs du framework**

- **Installation**

- **Avantages/Inconvénients**

- **Conclusion + chiffres**

- **Liens utiles**


*************************************************

La présentation étant **fournie et conscise sur mon slide**, je vous invite à y faire un tour ! 

<img src="/img/slevtejs/logo.png">

[Lien Slide SvelteJS](https://docs.google.com/presentation/d/1VcwGhvYPBRIbQiwF2i_cpdrA8n6w1EEdr-_SzoFTRzc/edit#slide=id.g5d7f889622_0_13)


*************************************************
## Installation:

<img src="/img/slevtejs/1.png">

<img src="/img/slevtejs/2.png">


## Avantages: 

- **Peu lourd** (10 fois moins que ReactJS)

-  Svelte est en **v3**. Ce qui vous garantira :

    - qu'il est **opérationnel**.

    - qu'il est **là pour durer**.

- Pas de **compilation à l'éxécution**.

- Meilleure *gestion du virtual DOM*.

## Inconvénients: 

- **Projet jeune** donc impact qui reste à voir.

- Prise en main *différente des autres.*

- Un bug dans une bibliothèque peut être **difficile à localiser et à corriger.**

- Rien ne garantit qu’un **correctif** sera **rapidement publié par l’équipe de développement.**

**Merci à vous** ! 
