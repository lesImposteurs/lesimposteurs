---
title: "GitKraken"
date: 2019-08-02T14:58:06+02:00
draft: false
description: "Veille sur GitKraken, outil Git avec interface graphique."
img: ""
author: "Thomas"
---
# GitKraken 

*************************************************

## Programme: 

<img src="/img/gitkraken/logo.png">

- **Présentation**

- **Quelques exemples** 

- **Limites/avantages du logiciel** 

- **Conclusion**

Git Kraken est disponible en multi-plateformes sur : 

- *MacOS*

- *Linux*

- *Windows*

*************************************************

Le 25 Novembre 2015, Axosoft a mis en ligne une interface graphique (gratuite mais non opensource) dédiée à Git.

<img src="/img/gitkraken/exemple.png">

Cette interface permet de : 

- utiliser **la méthode Git Flow**
- *push/pull*
- créer des **branches** et les **merge**
- **annuler/refaire** en un clic (*commits, branches, repo..*)
- lier directement nos dossiers à **GitHub**, **GitLab** ou **BitBucket**


*************************************************

### Limites : 

- On peut **vite oublier comment utiliser Git** (via terminal)

- Version **gratuite restreinte**

- Pas en **OpenSource**

- **Forte concurrence** d’autres clients avec interface graphique Git.

<img src="/img/gitkraken/gitkrak.gif">

### Avantages :

- Git devient “**facile**”.

- **Pas de commandes** à connaître.

- **Multi plateforme** (Mac, Windows, Linux).

- Historiques (*commits , push, etc..*).

- **Maintenance continue.**

- Rattachable à **GitHub/GitLab** et **BitBucket**.

- **Pas lourd** à DL (60 à 80 Mo selon les OS).

<img src="/img/gitkraken/kraken.png">

Pour plus d'infos sur le sujet retrouvez mon slide en-dessous : [GitKraken](https://docs.google.com/presentation/d/1fU-jfszIqsbKpRLNCchXGxDpEVh3fqnRuVF_84Pi_4o/edit#slide=id.p)


