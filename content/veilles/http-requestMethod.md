---
title : "HTTP Request Method"
date : 2019-08-05
draft: false
description: "Veille sur HTTP Request Method."
img: ""
author: "Marie"
---

#### HTTP Request Method

Bonjour,
Je vais vous parler des méthodes de requêtes HTTP, pour ça, vous devez d'abord avoir des notions du fonctionnement d'un serveur.

Lorsque l'on fait une requête à notre serveur, il decode cette dernière avant de nous renvoyer une réponse (quand vous charger votre script JavaScript par exemple).


<img src="/img/httprequest/HTTP.png" alt="" style="">


Les HTTP Request Method sont donc une manière d'interagir avec notre serveur (lui demander des informations, créer un post, supprimer...) grâce à des "verbes" de requête http.

En voici la liste :
	* GET : Récupération de données
	* HEAD : Récupération de données seulement sur l’en-tête
	* POST : Envoi une entité vers la ressource
	* PUT : Remplacement
	* DELETE : Supprimer
	* CONNECT : Etablit un tunnel vers le serveur
	* OPTIONS : Décrit les options de communications
	* TRACE :  Message Test aller/retour
	* PATCH : Applique des modifications partielles


Pour utiliser ses verbes, vous devrez respecter une certaine syntaxe ! En voici un exemple :

<img src="/img/httprequest/HTTP_RequestMessageExample.png" alt="" style="">