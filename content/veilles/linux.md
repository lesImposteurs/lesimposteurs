---
title: "Linux"
date: 2019-07-18
draft: false
description: "Veille sur Linux."
categories: [Veilles]
img: ""
author: "Boris et Elodie"
---

<img src="/img/linux/torval.png">

Au commencement il y eut UNIX, créé par les bureaux d'études d'AT&T en 1974.
A cette "époque" préhistorique, les ordinateurs n'étaient que de gros calculateurs peu puissants, très chers, et presque incapables de communiquer entre eux. les systèmes d’exploitation, véritables Tour de Babel, étaient incompatibles entre eux, chaque constructeur utilisant son propre système.
Unix dans cette jungle se détache et sa capacité à fonctionner sur tout type de machine lui permet d’attirer l’attention de l’ARPA (Advanced Research Projects Agency), organisme public américain fondé par Heisenhower dans les années 1950.
le but premier était de relier les centres militaires stratégiques afin que si un centre est détruit les autres soient capables de communiquer. Vive la guerre froide ! ARPANET était né, l’ancêtre de notre internet moderne.

<img src="/img/linux/linux.png">

L'ARPA stimula la recherche en informatique, via des grandes universités qui s'ajoutèrent progressivement au réseau (29 centres en 1972) et qui n’avaient pour contrainte que la transparence de leurs travaux : le code-source était né.
Pour cette raison, l'ARPA financait seulement les recherches en "open-source", littéralement source-ouverte. En gros, on vous vend le logiciel sous forme compilée pour votre ordinateur et sous forme de code-source pour vous, à cette époque utiliser un ordinateur équivaut à être programmateur.
Unix se développe jusqu’en 1986, à cette date l’ARPA devient inutile, la fabrique de logiciel devient une activité lucrative.
Très rapidement des sociétés privées comme Sun ou Hewlett-Packard développent leur propre version d’Unix mais incompatibles entre elles, et ils en deviennent propriétaires.

1984-1991 : lancement du projet GNU
Si on a pu croire à la fin du logiciel libre, un certain Richard STALLMANN (un des hommes les plus importants après Jésus), informaticien au MIT, crée en 1984 avec un noyau d'irréductibles la Free Software Foundation (FSF). Son projet (qualifié d'utopique à l'époque) : inventer un système d'exploitation libre et gratuit de type Unix, baptisé GNU.(attention, Bac + 5 exigé !) "N" pour "not", "U" pour "Unix", et "G" pour ... GNU ! D'où "GNU's not Unix" ("GNU n'est pas Unix"). Stallmann s'oppose totalement au concept de logiciel propriétaire "copyrighté" (le copyright est l'équivalent du droit d'auteur français) en proposant des logiciels "copyleftés" selon la Licence Publique Générale (LPG) ou GPL (General Public License). Le Chavez de l’informatique !

1991 : naissance du noyau Linux
Le projet GNU avance dans l'ombre jusqu'en 1990, mais il n'a encore fait fonctionner aucun ordinateur dans le monde... C'est en 1991 qu'un jeune étudiant finlandais en informatique, Linus Torvalds (l’équivalent d’un Dieu vivant) développe, à 21 ans, le noyau (ou kernel en Anglais) pour un système GNU. C'est la brique élémentaire qui manquait à l'édifice ! Aidé dans sa tâche par une communauté très active sur le tout jeune Internet (d'où l'intérêt de fournir le code source), Linus finalise en 1993 la version 1.0 de GNU/Linux. Distribuée gratuitement sur le web, le succès est fulgurant dans le milieu des serveurs .

<img src="/img/linux/tux.png">

Petit interlude pour vous expliquer le choix du pingouin en tant que logo de la marque…
En fait, contrairement à ce que beaucoup de gens pensent ce n’est pas un pingouin mais un manchot et il s'appelle Tux .Son nom est tout simplement la combinaison du nom de Linus Torvald et de unix. Ce choix s’inscrit donc à la fois dans une démarche d’héritage et de continuité à travers la mention d unix et la volonté de s’en dissocier totalement puisque le logo aurait été choisi car le pingouin avait un côté "gentil" .
Cela représentait l’idée que Torvalds se faisait de Linux, un système sympa pour aider les gens, pas un système agressif pour conquérir des parts de marché.

<img src="/img/linux/piguin.png">

Linux est un système d'exploitation libre, c'est à dire que vous êtes libre de l'utiliser, de le modifier et de le re-distribuer.
Différentes sociétés l'ont donc reprit et complété afin de distribuer un système d'exploitation à leur goût. C'est ce qu'on appelle les distributions.
Parmi les plus connues, citons RedHat, Fedora, Mandriva, Debian, Suse, Slackware, Gentoo, Xandros, Lycoris…


<img src="/img/linux/logiciel.png">

Alors puisque le sujet fait débat, rapide tour d’horizon pour citer les principales inconvénients et avantages que possèdent Linux par rapport à Window 

    -les jeux: La quasi totalité des jeux sont conçus pour Windows et ne fonctionnent pas sous Linux.
    Il existe bien des petits jeux gratuits mais pratiquement aucun grand éditeur ne crée de jeux pour Linux.
    Cependant, avec Wine, on peut contourner le problème et faire que certains jeux Windows fonctionnent sous Linux.

    -le portefeuille: Linux est gratuit tout comme la majorité des logiciels qu’il utilise 
    alors que le système de base de WIndow est payant
    et qu’acquérir la suite office nécessite de payer un supplément

    -la sécurité: le système de window est beaucoup plus vulnérable 
    aux adwares malwares et virus  que ne l’est Linux

on ne peut pas toucher au code de Window, mais avec Linux on peut modifier l’interface graphique et meme faire focntionner son systeme sans passer par la moindre interface graphique

    -la prise en main est considérée plus difficile.

Merci à vous. 
    



